<?php

$nama = $_POST['nama'];
$nomor = $_POST['nomor'];
$Tanggal = $_POST['Tanggal'];
$driver = $_POST['driver'];
$kantong = $_POST['info'];

?>

<html>
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <style>
    .buttonS{
      background-color:green;
      color: white;
    }
    </style>
    <script>
        function check() {
          confirm('Yakin?');
        }

    </script>
        <title>Menu</title>
        
    </head>
    <body>
    <div class="container">
  <div class="row">
    <div class="col-sm text-center m-3 p-5">
        <h2 class="p-3">~Data Ojol~</h2>
        <label for="NamaDriver">Nama Driver : </label>
        <p><?= $nama ?></p>
        <label for="NoHP">Nomor Telepon : </label>
        <p><?= $nomor ?></p>
        <label for="datepick">Tanggal Pesan : </label>
        <p><?= $Tanggal ?></p>
        <label for="Driver">Asal Driver : </label>
        <p><?= $driver ?></p>
        <label for="Kantong">Bawa Kantong Belanja : </label>
        <p><?= $kantong ?></p>
        <center>
        <button class="buttonS btn " ><a href="form.html" style="text-decoration:none; color:white;"> << Kembali </a> </button>
        </center>
    </div>
    <div class="col-sm text-center m-3 p-5">
        <h2 class="p-3">~Menu~</h2>
        <p>pilih menu</p>
        <form action="nota.php" name="pesanan" method="POST" onsubmit="check()">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" name="Cosu" value="28000" id="Cosu">
            <label class="form-check-label font-weight-bold pr-5 " for="CoklatSusu">
            Es Coklat Susu
            </label>
            <label class="form-check-label pl-3" for="CoklatSusu">
            Rp. 28.000
            </label>
            <hr>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" name="Suma" value="18000" id="Suma">
            <label class="form-check-label font-weight-bold pr-5 " for="SusuMatcha">
            Es Susu Matcha
            </label>
            <label class="form-check-label pl-3" for="SusuMatcha">
            Rp. 18.000
            </label>
            <hr>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" name="Sumo" value="15000" id="Sumo">
            <label class="form-check-label font-weight-bold pr-5 " for="SusuMojicha">
            Es Susu Mojicha
            </label>
            <label class="form-check-label pl-3" for="SusuMojicha">
            Rp. 15.000
            </label>
            <hr>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" name="Male" value="30000" id="Male">
            <label class="form-check-label font-weight-bold pr-5 " for="MatchaLatte">
            Es Matcha Latte
            </label>
            <label class="form-check-label pl-3" for="MatchaLatte">
            Rp. 30.000
            </label>
            <hr>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" name="Tasu" value="21000" id="Tasu">
            <label class="form-check-label font-weight-bold pr-5 " for="TaroSusu">
            Es Taro Susu
            </label>
            <label class="form-check-label pl-3" for="TaroSusu">
            Rp. 21.000
            </label>
            <hr>
        </div>
        <div class="form-group row">
            <label for="NoOrder" class="col-sm-2 col-form-label text-left">Nomor Order</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" id="NmrOrder" name="NoOrder" required>
            </div>
        </div><hr>
        <div class="form-group row">
            <label for="Nama" class="col-sm-2 col-form-label text-left">Nama Pemesan</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="NamaPemesan" name="Nama" required>
            </div>
        </div><hr>
        <div class="form-group row">
            <label for="Email" class="col-sm-2 col-form-label text-left">Email</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="Email" name="Email" required>
            </div>
        </div><hr>
        <div class="form-group row">
            <label for="Alamat" class="col-sm-2 col-form-label text-left">Alamat Order</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="Alamat" name="Alamat" required>
            </div>
        </div><hr>
        <fieldset class="form-group">
            <div class="row">
                <legend class="col-form-label col-sm-5 pt-0 text-left">Member </legend>
                <div class="col-sm-5">
                  <div class="form-check form-check-inline pr-5">
                    <input class="form-check-input" type="radio" name="Member" id="Ya" value="Ya">
                    <label class="form-check-label" for="Ya">
                      Ya
                    </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="Member" id="Tidak" value="Tidak">
                    <label class="form-check-label" for="Tidak">
                      Tidak
                    </label>
                  </div>
                </div>
            </div>
        </fieldset><hr>
        <div class="form-group row">
            <label for="MetodeBayar" class="col-sm-4 col-form-label text-left">Metode Pembayaran</label>
            <div class="col-sm-8">
            <select class="form-control" name="metodeBayar" id="MetodeBayar">
                <option value="">----pilih metode pembayaran----</option>
                <option value="Cash">Cash</option>
                <option value="E-Money (OVO/Gopay)">E-Money (OVO/Gopay)</option>
                <option value="Credit Card ">Credit Card </option>
                <option value="Lainnya">Lainnya</option>
            </select>
            </div>
        </div><hr>
        <input class="buttonS btn-lg btn-block" type="submit" name="Cetak" value="Cetak Struk">
        </form>
    </div>
  </div>
</div>
  
</body>
</html>