<?php
if(isset($_POST['Cetak']))
{
        $total=0;
        if(isset($_POST['Cosu'])){
        $harga1= $_POST['Cosu'];
        $total+= $harga1;
        }
        if(isset($_POST['Suma'])){
        $harga2= $_POST['Suma'];
        $total+= $harga2;
        }
        if(isset($_POST['Sumo'])){
        $harga3= $_POST['Sumo'];
        $total+= $harga3;
        }
        if(isset($_POST['Male'])){
        $harga4= $_POST['Male'];
        $total+= $harga4;
        }
        if(isset($_POST['Tasu'])){
        $harga5= $_POST['Tasu'];
        $total+= $harga5;
        } 
}
$NoOrder = $_POST['NoOrder'];
$Nama = $_POST['Nama'];
$Email = $_POST['Email'];
$Alamat = $_POST['Alamat'];
$Member = $_POST['Member'];
$metodeBayar = $_POST['metodeBayar'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
    <title>Nota</title>
</head>
<body>
<div class="container">
    <div class="row">
    <div class="col-sm text-center m-3 p-5">
        <h2 class="p-3">~Transaksi Pemesanan~</h2>
        <p>Terimakasih telah berbelanja dengan Kopi Susu Duarrr!</p>
        <h2>Rp. <?= $total ?> </h2>
        <div class="form-group row">
            <label for="nmrOrder" class="col-sm-2 col-form-label text-left">ID</label>
            <div class="col-sm-10">
                <p><?= $NoOrder ?> </p>
            </div>
        </div><hr>
        <div class="form-group row">
            <label for="namaPemesan" class="col-sm-2 col-form-label text-left">Nama</label>
            <div class="col-sm-10">
                <p><?= $Nama ?> </p>
            </div>
        </div><hr>
        <div class="form-group row">
            <label for="email" class="col-sm-2 col-form-label text-left">Email</label>
            <div class="col-sm-10">
                <p><?= $Email ?> </p>
            </div>
        </div><hr>
        <div class="form-group row">
            <label for="alamat" class="col-sm-2 col-form-label text-left">Alamat</label>
            <div class="col-sm-10">
                <p><?= $Alamat ?> </p>
            </div>
        </div><hr>
        <div class="form-group row">
            <label for="member" class="col-sm-2 col-form-label text-left">Member</label>
            <div class="col-sm-10">
                <p><?= $Member ?> </p>
            </div>
        </div><hr>
        <div class="form-group row">
            <label for="metodeBayar" class="col-sm-2 col-form-label text-left">Pembayaran</label>
            <div class="col-sm-10">
                <p><?= $metodeBayar ?> </p>
            </div>
        </div><hr>
    </div> 
    </div>
</div>
</body>
</html>